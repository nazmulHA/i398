package rpn;

import java.util.Stack;

public class RpnCalculator {
        private int accumulator = 0;
        Stack stack = new Stack();

        public int getAccumulator(){
            int value = accumulator;
            return value;
        }

        public void setAccumulator(int value) {
            accumulator = value;
        }

        public void enter(){
            stack.push(accumulator);
            accumulator = 0;
        }

        public void plus(){
            int lastElement = (int) stack.pop();
            accumulator = lastElement + accumulator;
        }

         public void minus(){
            int lastElement = (int) stack.pop();
            accumulator = lastElement - accumulator;
        }

        public void multiply(){
            int lastElement = (int) stack.pop();
            accumulator = lastElement * accumulator;
        }

        public int evaluate(String string){
            int value = Integer.parseInt(string);
            return value;
        }


    }

