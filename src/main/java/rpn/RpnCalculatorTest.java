package rpn;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    RpnCalculator c = new RpnCalculator();

    @Test

    public void defaultAccumulatorValueIs0() {
        assertThat(c.getAccumulator(), is(0));

    }

    @Test
    public void setAccumulatorValue(){
        c.setAccumulator(5);
        assertThat(c.getAccumulator(), is(5));
    }

    @Test
    public void calculatorSupportsAddition() {
        c.setAccumulator(1);
        c.enter();

        c.setAccumulator(2);
        c.plus();

        assertThat(c.getAccumulator(), is(3));
    }

    // Exercise 4 
    
    @Test
    public void calculatorSupportsSubtraction() {
        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(1);
        c.minus();

        assertThat(c.getAccumulator(), is(2));
    }

    @Test
    public void calculatorSupportsMultiplication(){
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);

        c.multiply();

        assertThat(c.getAccumulator(), is(12));

    }

    @Test
    // Check that calculator calculates correctly (4 + 3) * (2 + 1) = 21
    public void calculatorSupportsMultiplication2(){
        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();

        assertThat(c.getAccumulator(), is(21));

    }

    @Test
    public void calculatorSupportsRPN (){
        c.setAccumulator(5);
        c.enter();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();
        c.plus();
        c.enter();
        c.setAccumulator(3);
        c.plus();
        assertThat(c.getAccumulator(), is(20));
    }

}

