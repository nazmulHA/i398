package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack();
        assertThat(stack.size(), is(0));
    }

    @Test
    public void topElementofTheStack() {
        Stack stack = new Stack();

        stack.push(89);
        stack.push(92);

        assertThat(stack.peek(), is(92));

    }

    @Test
    public void stackofsizetwo(){

        Stack stack = new Stack();

        stack.push(89);
        stack.push(92);
        assertThat(stack.size(), is(2));
    }

    @Test
    public void popTwoElement(){

        Stack stack = new Stack();

        stack.push(89);
        stack.push(92);


        stack.pop();
        stack.pop();

        assertThat(stack.size(), is(0));
    }

    /*@Test public void poppedTwoElementwaspushed(){

        Stack stack = new Stack();

        stack.push(89);
        stack.push(92);


        stack.pop();
        stack.pop();

        assertThat(stack.push(T), hasItems(89));
    }*/

    @Test(expected = EmptyStackException.class)
    public void popOnemptyStack(){

        Stack stack = new Stack();
        stack.pop();
    }

    @Test //(expected = IllegalStateException.class)
    public void peekOnEmptyStack(){

        Stack stack = new Stack();
        stack.peek();
        assertThat(stack.peek(), is("null"));


    }
}