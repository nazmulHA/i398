package calc;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by nazmul on 12/3/2017.
 */
public class StringclacTest {
    private Stringclac calculator = new Stringclac();

    @Test
    public void shouldEmptyStringBe0(){
        assertEquals(0, calculator.add(""));
    }

    @Test
    public void shouldOneBe1(){
        String string = "1";
        int i = 1;
        checkAdd(string, 1);
    }

    @Test
    public void shouldCalculateAdd(){
        checkAdd("",0);
        checkAdd("1", 1);
        checkAdd("2", 2);
        checkAdd("1,2", 3);
    }

    private void checkAdd(String string, int i) {
        assertEquals(i, calculator.add(string));
    }

}